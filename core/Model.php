<?php

namespace core;

use PDO;
use SimpleLogger\File;

class Model
{
    protected static $db;

    private static $_pdo = null;

    private $loger;

    /**
     * Model constructor.
     */
    private function __construct()
    {

    }

    /**
     *
     */
    private function __clone()
    {
    }

    /**
     *
     */
    private function __wakeup()
    {
    }

    /**
     * @return Model|null|PDO
     */
    public static function getInstance()
    {
        if (self::$_pdo !== null) {
            return self::$_pdo;
        }

        //self::$_pdo = new self();
        try {
            self::$_pdo = new PDO(
                'mysql:host=' . CONFIG_DBSERVER . ';port=' . CONFIG_DBPORT . ';dbname=' . CONFIG_DATABASE,
                CONFIG_DBUSER, CONFIG_DBPASSWORD, [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]
            );
            self::$_pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
           // $this->loger = new File('log.txt');
        } catch (\PDOException $e) {
            print $e->getMessage();
            die();
        }

    }

    // a proxy to native PDO methods
    public function __call($method, $args)
    {
        return call_user_func_array(array(self::$_pdo, $method), $args);
    }

    // a helper function to run prepared statements smoothly
    /**
     * @param $sql
     * @param array $args
     * @return \PDOStatement
     */
    public function run($sql, $args = [])
    {
        $stmt = self::$_pdo->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }

    /** несколько запросов в транзакции
     * @param array $sql_args
     * [
     *    0 => ['sql' => 'insert ...', 'args' => [] ]
     *    1 => ['sql' => 'update ...', 'args' => ['id',] ]
     * ]
     * @param boolean $withTrans
     * @return bool
     * @throws \Exception
     */
    public function save($sql_args, $withTrans = false)
    {
        try {
            if ($withTrans) {
                self::$_pdo->beginTransaction();
            }
            $isError = false;
            foreach ($sql_args as $sql_arg) {
                $stmt = self::$_pdo->prepare($sql_arg['sql']);
                if (!$stmt->execute($sql_arg['args'])) {
                    $this->loger->log('1', implode(',', self::$_pdo->errorInfo()));
                    $isError = true;
                }
            }
            if ($withTrans) {
                return self::$_pdo->commit();
            }
            return !$isError;
        } catch (\Exception $e) {
            if ($withTrans) {
                self::$_pdo->rollBack();
            }
            throw $e;
        }
    }



    /** генерация условий запроса
     * @param array $data
     * @param string $glue
     * @return string
     */
    private static function combine($data = array(), $glue = ' AND ')
    {
        if (!isset($data)) return '1 = 1';
        return implode($glue, array_map(function ($k, $v) {
            if ($v === '0000-00-00 00:00:00') {
                $v = 'NULL';
            } elseif (!is_numeric($v)) {
                $v = "'$v'";
            }
            return "`$k` = $v";
        }, array_keys($data), $data));
    }

    /** генерация полей выборки запроса
     * @param  mixed $fieldNames
     * @return mixed|string
     */
    protected function createFieldNames($fieldNames)
    {
        if (is_string($fieldNames)) {
            if ($fieldNames != '*') {
                $fieldNames = str_replace('`', '', $fieldNames);
                $fieldNames = '`' . $fieldNames . '`';
            }
        } elseif (is_array($fieldNames)) {
            foreach ($fieldNames as &$fieldName) {
                $fieldName = str_replace('`', '', $fieldName);
                $fieldName = '`' . $fieldName . '`';
            }
            $fieldNames = implode(',', $fieldNames);
        }
        return $fieldNames;
    }

    /** выборка по полю id
     * @param integer $id
     * @param mixed $fieldNames
     * @return mixed
     */
    public function find($id, $fieldNames = '*')
    {
        $fieldNames = $this->createFieldNames($fieldNames);
        /** @var string $TABLE_NAME */
        return $this->run(
            'SELECT ' . $fieldNames . ' FROM ' . static::$TABLE_NAME . ' WHERE id = ?', [$id])->fetch();
    }

    /**
     * @param $id
     * @param string $fieldNames
     * @return array
     */
    public function findAllById($id, $fieldNames = '*')
    {
        $fieldNames = $this->createFieldNames($fieldNames);
        /** @var string $TABLE_NAME */
        return $this->run(
            'SELECT ' . $fieldNames . ' FROM ' . static::$TABLE_NAME . ' WHERE id = ? ORDER BY 1 DESC', [$id])->fetchAll();
    }

    /**
     * @param string $fieldNames
     * @return array
     */
    public function findAll($fieldNames = '*')
    {
        $fieldNames = $this->createFieldNames($fieldNames);
        return $this->run('SELECT ' . $fieldNames . ' FROM ' . static::$TABLE_NAME . ' ORDER BY 1 DESC', [])->fetchAll();
    }

    /**
     * @param array $conditions
     * @param mixed $fieldNames
     * @return string
     */
    public function findByAttrs($conditions = [], $fieldNames = '*')
    {
        $fieldNames = $this->createFieldNames($fieldNames);
        return $this->run(
            ' SELECT ' . $fieldNames .
            ' FROM `' . static::$TABLE_NAME .
            '` WHERE ' . self::combine($conditions), [])->fetch();
    }

    /**
     * @param string $fieldName
     * @return int
     */
    public function getCount($fieldName = '*')
    {
        return $this->run('SELECT ' . $fieldName . ' FROM ' . static::$TABLE_NAME, [])->rowCount();
    }

    /**
     * @param $limit
     * @param $offset
     * @return mixed
     */
    public function findWithLimit($limit = 3, $offset = 0,  $nameSortField = '1', $direction = 'desc')
    {
        return $this->run(
            ' SELECT * ' .
            ' FROM `' . static::$TABLE_NAME .
            "` ORDER BY $nameSortField $direction LIMIT $limit OFFSET $offset", [])
            ->fetchAll();
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->run('DELETE FROM ' . static::$TABLE_NAME . ' WHERE id =  ' . (int)$id, [])->rowCount();
    }
}
