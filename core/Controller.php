<?php

namespace core;


class Controller
{
    public $layout = 'default';

    private $controllerName;

    public function __construct()
    {
        $this->controllerName = strtolower(str_replace('Controller', '', get_class($this)));
    }


    /**
     * @param string $view
     * @param array $data
     */
    public function render($view, $data = [])
    {
        $view = DIR_APP_VIEWS . DIRECTORY_SEPARATOR . $this->controllerName . DIRECTORY_SEPARATOR . $view . '.php';
        include_once DIR_APP_VIEWS . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . $this->layout . '.php';
    }

    public function renderPart($view, $data = [])
    {
        $view = DIR_APP_VIEWS . DIRECTORY_SEPARATOR . $this->controllerName . DIRECTORY_SEPARATOR . $view . '.php';
    }
}