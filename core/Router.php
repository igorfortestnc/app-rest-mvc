<?php

namespace core;


class Router
{
    /**
     *
     */
    public static function Run()
    {
        define('DIR_APP_CONTROLLERS', APPLICATION_ROOT . DIRECTORY_SEPARATOR . 'controllers');
        define('DIR_APP_VIEWS', APPLICATION_ROOT . DIRECTORY_SEPARATOR . 'views');
        $controllerName = 'main';
        $actionName = 'index';
        $route_array = explode('/', $_SERVER['REQUEST_URI']);
        if (!empty($route_array[1])) {
            $controllerName = strtolower($route_array[1]);
        }
        if (!empty($route_array[2])) {
            $actionName = strtolower($route_array[2]);
        }
        $controllerName = ucfirst($controllerName) . 'Controller';
        $actionName = 'action' . ucfirst($actionName);
        $fileNameController = DIR_APP_CONTROLLERS . DIRECTORY_SEPARATOR . $controllerName . '.php';
        if (file_exists($fileNameController)) {
            include_once $fileNameController;
        } else {
            header('Location: /error/404');
            exit;
        }
        $controller = new $controllerName();
        if (!method_exists($controller, $actionName)) {
            header('Location: /error/404');
            exit;
        }
        $controller->$actionName();
    }
}