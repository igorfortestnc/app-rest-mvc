## Engine "My Native MVC and Rest Service"

- 	One entry point;
- 	Work with DB using PDO;
- 	Support API;
- 	Support MVC;
- 	Composer

Entities:


Connecting write into _ROOT/config/db.php_


```
CREATE TABLE brands (
  id int(11) NOT NULL,
  name varchar(50) NOT NULL,
  brand_logo varchar(255) DEFAULT NULL,
  category_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;



CREATE TABLE categories (
  id int(11) NOT NULL AUTO_INCREMENT,
  ids varchar(24) NOT NULL,
  name varchar(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;


CREATE TABLE products (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  image varchar(255) DEFAULT NULL,
  brand_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;
ROW_FORMAT = DYNAMIC;
```