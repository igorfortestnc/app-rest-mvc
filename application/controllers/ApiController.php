<?php

use core\Controller;


class ApiController extends Controller
{
    public function actionTasks()
    {
        try {
            $api = new TasksApi();
            echo $api->run();
        } catch (Exception $e) {
            echo json_encode(Array('error' => $e->getMessage()));
        }
    }

    public function actionCategories()
    {
        try {
            $api = new CategoriesApi();
            echo $api->run();
        } catch (Exception $e) {
            echo json_encode(['error' => $e->getMessage()]);
        }
    }

    public function actionBrands()
    {
        try {
            $api = new BrandsApi();
            echo $api->run();
        } catch (Exception $e) {
            echo json_encode(['error' => $e->getMessage()]);
        }
    }

    public function actionProducts()
    {
        try {
            $api = new ProductsApi();
            echo $api->run();
        } catch (Exception $e) {
            echo json_encode(['error' => $e->getMessage()]);
        }
    }

    public function actionShowcase()
    {
        try {
            $api = new ShowcaseApi();
            echo $api->run();
        } catch (Exception $e) {
            echo json_encode(Array('error' => $e->getMessage()));
        }
    }
}