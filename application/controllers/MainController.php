<?php
/**
 * This is file for testing & an example supporing of MVC
 */
use application\models\Task;
use core\Controller;
use application\models\User;

class MainController extends Controller
{
    const COUNT_ROW_PER_PAGE = 3;

    /**
     * управление и рендер баланса
     *
     * @throws Exception
     */
    public function actionIndex()
    {
        $numActivePage = Helper::iniPOST('page_num', 0);
        $User = new User();
        $Task = new Task();
        $allRowCount = $Task->getCount();
        $totalPages = ceil($allRowCount / self::COUNT_ROW_PER_PAGE);
        $nameSortField = Helper::iniPOST('name_sort_field', '1');
        $direction = Helper::iniPOST('direction', 'desc');

        $numActivePage = ($numActivePage == 0) ? 1 : $numActivePage;
        $dataTask = [
            'data'          =>
                $Task->findWithLimit(self::COUNT_ROW_PER_PAGE, ($numActivePage - 1) * self::COUNT_ROW_PER_PAGE, $nameSortField, $direction),
            'allRowCount'   => $allRowCount,
            'totalPages'    => $totalPages,
            'numActivePage' => $numActivePage
        ];

        if  (Helper::iniPOST('is_ajax', 0)) {
            echo json_encode($dataTask);
        } else {
            if (isset($_SESSION['udata'])) {
              //  $error = $User->validator->checkPrice($ammount);
            }
            $this->render('index', [
                'dataUser'      => $User->find((int)$_SESSION['udata']['id']),
                'dataTask'      => $dataTask
            ]);
        }
    }

    /**
     * @throws Exception
     */
    public function actionTask()
    {
        $name_author = Helper::iniPOST('name_author', '');
        $Task = new Task();
        $dataTask = [];
        $type = Helper::iniPOST('type', 'save');
        $id = Helper::iniPOST('id', 0);
        if ((preg_match('/\/main\/task\/(\d*)/i', $_SERVER['REQUEST_URI'], $matches) > 0) && ($type != 'edit')) {
            $dataTask = $Task->find($matches[1]);
            $type = 'edit';
            $id = $matches[1];
        } else {
            // сохранение
            if (!empty($name_author)) {
                if ($type == 'edit') {
                    if ($Task->editTask($_POST)) {
                        Helper::absRedirect('/main/index');
                    }
                } else {
                    if ($Task->saveTask($_POST)) {
                        Helper::absRedirect('/main/index');
                    }
                }
            }
        }
        $this->render('task', ['dataTask' => $dataTask, 'type' => $type, 'id' => $id]);
    }

    /**
     * авторизация
     */
    public function actionLogin()
    {
        $login = Helper::iniPOST('login', '');
        if (!empty($login)) {
            if (!isset($_SESSION['udata'])) {
                $user = new User();
                $result = $user->getUserData([
                    'username' => $login,
                    'password_hash' => Helper::iniPOST('password')
                ]);
                if (is_array($result))
                    $_SESSION['udata'] = $result;
            }
            if (isset($_SESSION['udata'])) {
                session_write_close();
                Helper::absRedirect('/main/index');
                return;
            }
        }
        $this->render('login');
    }

    /**
     * выход
     */
    public static function actionLogout()
    {
        session_destroy();
        session_unset();
        Helper::absRedirect('/');
    }
}

