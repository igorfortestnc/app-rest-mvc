<?php

use application\models\Task;

class TasksApi extends Api
{

    public $apiName = 'tasks';


    /**
     * Метод GET
     * Вывод списка всех записей
     * http://ДОМЕН/users
     * http://localhost:81/api/tasks
     * @return string
     */
    public function indexAction()
    {
        echo 'indexAction';
        $Task = new Task();
        $tasks = $Task->findAll();
        if($tasks){
            echo $this->response($tasks, 200);
        }
        echo $this->response('Data not found', 404);
    }


    protected function viewAction()
    {
        // TODO: Implement viewAction() method.
        echo 'viewAction';
    }

    protected function createAction()
    {
        echo 'createAction';
    }

    protected function updateAction()
    {
        echo 'updateAction';
    }

    protected function deleteAction()
    {
        echo 'deleteAction';
    }
}