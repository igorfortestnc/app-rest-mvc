<?php

use application\models\Category;

class CategoriesApi extends Api
{

    public $apiName = 'categories';

    /**
     * Method GET
     * Show all records
     * http://domain/api/categories
     * @return string
     */
    public function indexAction()
    {
        $Category = new Category();
        $categories = $Category->findAll();
        $resCategories = $this->prepareArray($categories);
        if ($resCategories){
            return $this->response($resCategories, 200);
        }
        return $this->response('Data not found', 404);

    }

    /**
     * Method GET
     * Viewing of record (by id)
     * http://domain/categories/1
     * @return string
     */
    protected function viewAction()
    {
        //id должен быть первым параметром после /categories/x
        $id = array_shift($this->requestUri);
        if($id){
            $Category = new Category();
            $category = $Category->find((int)$id);
            if($category){
                return $this->response($category, 200);
            }
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method POST
     * Create a new record
     * http://domain/categories + параметры запроса name
     * @return string
     * @throws Exception
     */
    protected function createAction()
    {
        $Category = new Category();
        $arr = [
            'name' => Helper::iniJsonPOST('name', ''),
            'ids'  => md5(rand(1, 100))
        ];
        if (!empty($arr['name'])) {
            $saved = $Category->saveCommon($arr);
        }
        if ($saved) {
            return $this->response($saved, 200);
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method PUT
     * Updating of record (by id)
     * http://domain/categories/1 + параметры запроса name
     * @return string
     * @throws Exception
     */
    protected function updateAction()
    {
        $id = array_shift($this->requestUri);
        if($id) {
            $Category = new Category();
            $arr = [
                'name' => Helper::iniJsonPUT('name', ''),
                'id'   => $id
            ];
            $saved = $Category->editCommon($arr);
            if ($saved) {
                return $this->response($saved, 200);
            }
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method DELETE
     * Deleting of record (by id)
     * http://domain/categories/1
     * @return string
     */
    protected function deleteAction()
    {
        $id = array_shift($this->requestUri);
        if($id) {
            $Category = new Category();
            return $this->response($Category->delete($id), 200);
        }
        return $this->response('Data not found', 404);
    }
}