<?php

use application\models\Category;
use application\models\Brand;
use application\models\Product;

class ShowcaseApi extends Api
{
    public $apiName = 'showcase';

    /**
     * Method GET
     * Show all records
     * http://domain/api/showcase
     * @return string
     */
    public function indexAction()
    {
        $showcase = [];
        $Category = new Category();
        $Brand    = new Brand();
        $Product  = new Product();
        $categories = $Category->findAll();

        foreach ($categories as $catKey => $category) {
            $category = array_filter($category, function($k) {
                return is_string($k);
            }, ARRAY_FILTER_USE_KEY);
            $showcase['categories'][$catKey] = $category;
            $brands = $Brand->findAllById($category['id']);
            foreach ($brands as $brandKey => $brand) {
                $brand = array_filter($brand, function($k) {
                    return is_string($k);
                }, ARRAY_FILTER_USE_KEY);
                $showcase['categories'][$catKey]['brands'][] = $brand;
                $products = $Product->findAllById($brand['id']);
                foreach ($products as $product) {
                    $product = array_filter($product, function($k) {
                        return is_string($k);
                    }, ARRAY_FILTER_USE_KEY);
                    $showcase['categories'][$catKey]['brands'][$brandKey]['products'][] = $product;
                }
            }
        }
        if ($showcase){
            return $this->response($showcase, 200);
        }
        return $this->response('Data not found', 404);

    }

    /**
     * Method GET
     * Viewing of record (by id)
     * http://domain/showcase/1
     * @return string
     */
    protected function viewAction()
    {
        //id должен быть первым параметром после /showcase/x
        $id = array_shift($this->requestUri);
        if($id){

        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method POST
     * Create a new record
     * http://domain/showcase + параметры запроса name
     * @return string
     * @throws Exception
     */
    protected function createAction()
    {

        return $this->response('Data not found', 404);
    }

    /**
     * Method PUT
     * Updating of record (by id)
     * http://domain/showcase/1 + параметры запроса name
     * @return string
     * @throws Exception
     */
    protected function updateAction()
    {
        $id = array_shift($this->requestUri);
        if($id) {

        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method DELETE
     * Deleting of record (by id)
     * http://domain/showcase/1
     * @return string
     */
    protected function deleteAction()
    {
        $id = array_shift($this->requestUri);
        if($id) {

        }
        return $this->response('Data not found', 404);
    }
}