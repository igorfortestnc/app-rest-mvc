<?php

use application\models\Product;

class ProductsApi extends Api
{

    public $apiName = 'products';

    /**
     * Method GET
     * Show all records
     * http://domain/api/products
     * @return string
     */
    public function indexAction()
    {
        $Product = new Product();
        $products = $Product->findAll();
        $resProducts = $this->prepareArray($products);
        if ($resProducts){
            return $this->response($resProducts, 200);
        }
        return $this->response('Data not found', 404);

    }

    /**
     * Method GET
     * Viewing of record (by id)
     * http://domain/products/1
     * @return string
     */
    protected function viewAction()
    {
        //id должен быть первым параметром после /products/x
        $id = array_shift($this->requestUri);
        if($id){
            $Product = new Product();
            $product = $Product->find((int)$id);
            if($product){
                return $this->response($product, 200);
            }
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method POST
     * Create a new record
     * http://domain/products + param of request name
     * @return string
     * @throws Exception
     */
    protected function createAction()
    {
        $Product = new Product();
        $arr = [
            'name'         => Helper::iniJsonPOST('name', 'test name prod'),
            'image'   => Helper::iniJsonPOST('image', ''),
            'brand_id'  => Helper::iniJsonPOST('brand_id', 0)
        ];
        $saved = $Product->saveCommon($arr);
        if ($saved) {
            return $this->response($saved, 200);
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method PUT
     * Updating of record (by id)
     * http://domain/products/1 + param of request name
     * @return string
     * @throws Exception
     */
    protected function updateAction()
    {
        $id = array_shift($this->requestUri);
        if($id) {
            $Product = new Product();
            $arr = [
                'name' => Helper::iniJsonPUT('name', 'test name'),
                'id'   => $id
            ];
            $saved = $Product->editCommon($arr);
            if ($saved) {
                return $this->response($saved, 200);
            }
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method DELETE
     * Deleting of record (by id)
     * http://domain/products/1
     * @return string
     */
    protected function deleteAction()
    {
        $id = array_shift($this->requestUri);
        if($id) {
            $Product = new Product();
            return $this->response($Product->delete($id), 200);
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method POST
     * Upload image
     * http://domain/products/1 + param of request product_image
     * @return string
     * @throws Exception
     */
    protected function uploadAction()
    {
        $id = array_shift($this->requestUri);
        if($id) {
            $uploadDir = '/var/www/html/uploads/';
            if (move_uploaded_file($_FILES['product_image']['tmp_name'], $uploadDir . $_FILES['product_image']['name'])) {
                $Product = new Product();
                $arr = [
                    'image' => '/uploads/' . $_FILES['product_image']['name'],
                    'id'   => $id
                ];
                $saved = $Product->editCommon($arr);
                if ($saved) {
                    return $this->response($saved, 200);
                }
            }
        }
        return $this->response('Data not found', 404);
    }
}