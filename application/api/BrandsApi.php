<?php

use application\models\Brand;

class BrandsApi extends Api
{

    public $apiName = 'brands';

    /**
     * Method GET
     * Show all records
     * http://domain/api/brands
     * @return string
     */
    public function indexAction()
    {
        $Brand = new Brand();
        $brands = $Brand->findAll();
        $resBrands = $this->prepareArray($brands);
        if ($resBrands){
            return $this->response($resBrands, 200);
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method GET
     * Viewing of record (by id)
     * http://domain/brands/1
     * @return string
     */
    protected function viewAction()
    {
        //id должен быть первым параметром после /brands/x
        $id = array_shift($this->requestUri);
        if($id){
            $Brand = new Brand();
            $brand = $Brand->find((int)$id);
            if($brand){
                return $this->response($brand, 200);
            }
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method POST
     * Create a new record
     * http://domain/brands + параметры запроса name, brand_logo, category_id
     * @return string
     * @throws Exception
     */
    protected function createAction()
    {
        $Brand = new Brand();
        $arr = [
            'name'         => Helper::iniJsonPOST('name', 'test name'),
            'brand_logo'   => Helper::iniJsonPOST('brand_logo', ''),
            'category_id'  => Helper::iniJsonPOST('category_id', 0)
        ];
        $saved = $Brand->saveCommon($arr);
        if ($saved) {
            return $this->response($saved, 200);
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method PUT
     * Updating of record (by id)
     * http://domain/brands/1 + параметры запроса name, brand_logo, category_id
     * @return string
     * @throws Exception
     */
    protected function updateAction()
    {
        $id = array_shift($this->requestUri);
        if($id) {
            $Brand = new Brand();
            $arr = [
                'name'         => Helper::iniJsonPUT('name', 'test name'),
                'brand_logo'   => Helper::iniJsonPUT('brand_logo', ''),
                'category_id'  => Helper::iniJsonPUT('category_id', 0),
                'id'           => $id
            ];
            $saved = $Brand->editCommon($arr);
            if ($saved) {
                return $this->response($saved, 200);
            }
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method DELETE
     * Deleting of record (by id)
     * http://domain/brands/1
     * @return string
     */
    protected function deleteAction()
    {
        $id = array_shift($this->requestUri);
        if($id) {
            $Brand = new Brand();
            return $this->response($Brand->delete($id), 200);
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Method POST
     * Upload image
     * http://domain/brands/1 + param of request product_image
     * @return string
     * @throws Exception
     */
    protected function uploadAction()
    {
        $id = array_shift($this->requestUri);
        if($id) {
            $uploadDir = '/var/www/html/uploads/';
            if (move_uploaded_file($_FILES['product_image']['tmp_name'], $uploadDir . $_FILES['product_image']['name'])) {
                $Brand = new Brand();
                $arr = [
                    'brand_logo' => '/uploads/' . $_FILES['product_image']['name'],
                    'id'   => $id
                ];
                $saved = $Brand->editCommon($arr);
                if ($saved) {
                    return $this->response($saved, 200);
                }
            }
        }
        return $this->response('Data not found', 404);
    }
}