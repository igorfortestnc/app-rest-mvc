<?php

namespace application\models;

use core\Model;

class CommonModel extends Model
{
    protected $table = '';

    /**
     * @var \Validator
     */
    public $validator;
    /**
     * @var
     */
    public $errors;

    /**
     * @var array
     */
    public $rules = [];

    /**
     * Model constructor.
     */
    public function __construct()
    {
        self::$db = $this->getInstance();
        $this->validator = new \Validator($this->rules);
    }

    /**
     * @param array $arr
     * @return array
     */
    protected function getFieldValues($arr) {
        unset($arr['id']);
        $arr_keys = array_keys($arr);
        $fieldStr = implode(' = ?, ', $arr_keys) . ' = ?';
        $arr_vals = array_values($arr);
        return [
            'fieldStr' => $fieldStr,
            'arr_vals' => $arr_vals
        ];
    }

    /**
     * Save from post
     *
     * @param array $arr
     * @return bool
     * @throws \Exception
     */
    public function saveCommon($arr)
    {
        $fldVals = $this->getFieldValues($arr);
        $sql_with_args = [
            0 => [
                'sql' => 'insert ' . $this->table . ' set ' . $fldVals['fieldStr'],
                'args' => $fldVals['arr_vals']
            ],
        ];
        return $this->save($sql_with_args);
    }
    /**
     *
     * @param $arr
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function editCommon($arr)
    {
        $id = $arr['id'];
        $fldVals = $this->getFieldValues($arr);
        $sql_with_args = [
            0 => [
                'sql' => 'update ' . $this->table . ' set ' . $fldVals['fieldStr'] . ' where id = ' . $id,
                'args' => $fldVals['arr_vals']
            ],
        ];
        return $this->save($sql_with_args);
    }



}