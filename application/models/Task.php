<?php

namespace application\models;


class Task extends CommonModel
{
    /**
     * @var string
     */
    public static $TABLE_NAME = 'task';

    /**
     * Task constructor.
     */
    public function __construct()
    {
        // правило будет использовано при регистрации
        $this->rules = [
            'table'        => 'task',
        ];
        return parent::__construct();
    }

    /**
     * валидация
     * @param array $data
     * @return bool
     */
    public function validate($data)
    {
        $errors = [

        ];

        if (false === $this->validator->checkErrors($errors))
            return true;
        else {
            $this->errors = $errors;
            return false;
        }
    }

    /**
     * @param array $arr
     * @return array
     */
    public function getFieldValues($arr)
    {
        $arr['is_done'] = ($arr['is_done'] == 'Да') ? 1 : 0;
        unset($arr['id']); unset($arr['type']);
        $arr_keys = array_keys($arr);
        $fieldStr = implode(' = ?, ', $arr_keys) . ' = ?';
        $arr_vals = array_values($arr);
        return [
           'fieldStr' => $fieldStr,
           'arr_vals' => $arr_vals
        ];
    }

    /**
     * @param array $arr
     * @return bool
     * @throws \Exception
     */
    public function saveTask($arr)
    {
        $fldVals = $this->getFieldValues($arr);
        $sql_with_args = [
            0 => [
                'sql' => 'insert ' . self::$TABLE_NAME . ' set ' . $fldVals['fieldStr'],
                'args' => $fldVals['arr_vals']
            ],
        ];
        return $this->save($sql_with_args);
    }

    /**
     * @param $arr
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function editTask($arr)
    {
        $fldVals = $this->getFieldValues($arr);
        $sql_with_args = [
            0 => [
                'sql' => 'update ' . self::$TABLE_NAME . ' set ' . $fldVals['fieldStr'] . ' where id = ' . $arr['id'],
                'args' => $fldVals['arr_vals']
            ],
        ];
        return $this->save($sql_with_args);
    }

    /**
     * @param $conditionals
     * @return string
     */
    public function getTaskData($conditionals)
    {
        return $this->findByAttrs($conditionals);
    }

}