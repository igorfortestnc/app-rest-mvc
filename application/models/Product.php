<?php

namespace application\models;

include_once 'CommonModel.php';

class Product extends CommonModel
{
    /**
     * @var string
     */
    public static $TABLE_NAME = 'products';

    /**
     * constructor.
     */
    public function __construct()
    {
        $this->table = self::$TABLE_NAME;
        return parent::__construct();
    }

}