<?php

namespace application\models;


class User extends CommonModel
{
    /**
     * @var string
     */
    public static $TABLE_NAME = 'user';

    /**
     * User constructor.
     */
    public function __construct()
    {
        // правило будет использовано при регистрации
        $this->rules = [
            'table'        => 'user', // Таблица регистрации
            'min_login'    => 4,      // Минимальная длина логина
            'max_login'    => 45,     // Максимальная длина логина
            'min_password' => 1,      // Минимальная длина пароля
        ];
        return parent::__construct();
    }

    /**
     * валидация
     * @param array $data
     * @return bool
     */
    public function validate($data)
    {
        $errors = [
            1 => $this->validator->checkLogin($data['email']),
            2 => $this->validator->checkPassword($data['password'], $data['repeat']),
            3 => $this->validator->checkEmail($data['email']),
        ];

        if (false === $this->validator->checkErrors($errors))
            return true;
        else {
            $this->errors = $errors;
            return false;
        }
    }

    /**
     * @param $conditionals
     * @return string
     */
    public function getUserData($conditionals)
    {
        return $this->findByAttrs($conditionals);
    }
}