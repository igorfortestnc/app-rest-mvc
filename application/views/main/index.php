<div class="btn btn-outline-success my-2 my-sm-0"><a style="text-underline: none; color: #007bff" href="/main/task">Создать задачу</a></div>
<table class="table table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Идентификатор</th>
        <th><a href="#" onclick="sortByField(<?= isset($_SESSION['udata']) ? 1 : 0 ?>, event, this);"
               data-direction="desc" data-sort-field="name_author">Автор</a></th>
        <th><a href="#" onclick="sortByField(<?= isset($_SESSION['udata']) ? 1 : 0 ?>, event, this);"
               data-direction="desc" data-sort-field="email_author">Email</a></th>
        <th>Заголовок</th>
        <th>Описание</th>
        <th><a href="#" onclick="sortByField(<?= isset($_SESSION['udata']) ? 1 : 0 ?>, event, this);"
               data-direction="desc" data-sort-field="is_done">Проверено</a></th>
        <? if (isset($_SESSION['udata'])): ?>
            <th>Управление</th>
        <? endif; ?>
    </tr>
    </thead>
    <tbody>
    <? foreach ($data['dataTask']['data'] as $task): ?>
        <tr>
            <th scope="row"><?= ++$i ?></th>
            <td><?= $task['id'] ?></td>
            <td><?= $task['name_author'] ?></td>
            <td><?= $task['email_author'] ?></td>
            <td><?= $task['title'] ?></td>
            <td><?= $task['description'] ?></td>
            <td><?= $task['is_done'] == 1 ? 'Да' : 'Нет' ?></td>
            <? if (isset($_SESSION['udata'])): ?>
                <td><a href="/main/task/<?= $task['id'] ?>">Редактировать</a></td>
            <? endif; ?>
        </tr>
    <? endforeach; ?>
    </tbody>
</table>
<div id="page-selection"></div>
<script>
    $('#page-selection').bootpag({
        total: <?= $data['dataTask']['totalPages'] ?>,
        page: <?= $data['dataTask']['numActivePage'] ?>,
        maxVisible: 3,
        leaps: true,
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function(event, num){
        getTaskData(num, <?= isset($_SESSION['udata']) ? 1 : 0 ?>,
            $('a .active_sort').attr('data-sort-field'),
            $('a .active_sort').attr('data-direction')
        );
        numPage = num;
    });
</script>
