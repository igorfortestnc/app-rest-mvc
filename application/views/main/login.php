<style>
    body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #eee;
    }

    .form-signin {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }

    .form-signin .form-signin-heading,
    .form-signin .checkbox {
        margin-bottom: 10px;
    }

    .form-signin .checkbox {
        font-weight: 400;
    }

    .form-signin .form-control {
        position: relative;
        box-sizing: border-box;
        height: auto;
        padding: 10px;
        font-size: 16px;
    }

    .form-signin .form-control:focus {
        z-index: 2;
    }

    .form-signin input[type="text"] {
        margin-bottom: 5px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }

    .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

</style>
<form class="form-signin" method="post">
    <h2 class="form-signin-heading">Вход в систему</h2>
    <input type="text" name="login" id="inputName" class="form-control" placeholder="Логин" required autofocus>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Пароль" required>
    <button class="btn btn-lg btn-primary" type="submit">Как администратор</button>
    <div class="btn btn-lg btn-primary"><a style="text-underline: none; color: #eeeeee" href="/main/index">Гость</a></div>
</form>