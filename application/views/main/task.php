<form method="post" action="task">
    <div class="form-group">
        <label for="formGroupExampleInput">Автор</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="name_author" placeholder="Имя автора"
               value="<?= $data['dataTask']['name_author'] ?>">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Email</label>
        <input type="email" class="form-control" id="exampleFormControlInput1" name="email_author"
               placeholder="name@example.com" value="<?= $data['dataTask']['email_author'] ?>">
    </div>
    <? if (isset($_SESSION['udata'])): ?>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Проверено</label>
            <select class="form-control" id="exampleFormControlSelect1" name="is_done">
                <option <?= $data['dataTask']['is_done'] == 0 ? 'selected' : '' ?> >Нет</option>
                <option <?= $data['dataTask']['is_done'] == 1 ? 'selected' : '' ?> >Да</option>
            </select>
        </div>
    <? endif; ?>
    <div class="form-group">
        <label for="formGroupExampleInput2">Заголовок</label>
        <input type="text" class="form-control" id="formGroupExampleInput2" name="title" placeholder="Название задачи"
               value="<?= $data['dataTask']['title'] ?>">
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Описание</label>
        <textarea class="form-control" id="exampleFormControlTextarea1"
                  name="description" rows="3"><?= $data['dataTask']['description'] ?></textarea>
    </div>
    <div class="form-group">
        <button class="btn btn-lg btn-primary" type="submit">Сохранить</button>
        <div class="btn btn-lg btn-primary"><a style="text-underline: none; color: #eeeeee" href="/main/index">Назад</a></div>
    </div>
    <input type="hidden" name="type" value="<?= $data['type'] ?>">
    <input type="hidden" name="id" value="<?= $data['id'] ?>">
</form>