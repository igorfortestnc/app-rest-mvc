function normalizeUrl(pathname)
{
    var names = pathname.split('/');
    if (window.location.href.indexOf(names[0]) > -1) {
        return names[1];
    }
    return pathname;
}

var numPage;

function getTaskData(num, logined, sortField, direction)
{
    var num = typeof(num) === 'undefined' ? 1 : num;
    $.post(normalizeUrl('main/index'), {page_num: num, is_ajax: true, name_sort_field: sortField, direction: direction})
        .done(function (data) {
            var tasks = JSON.parse(data);
            $('tbody').remove();
            var  tbody = '';
            for (var i = tasks.data.length - 1; i >=0; i--) {
                var strEdit = '';
                if (logined) {
                    strEdit = '<td><a href="/main/task/' + tasks.data[i].id + '">Редактировать</a></td>';
                }
                tbody =
                    '<tr>' +
                    '<th scope="row">'+(i+1)+'</th>' +
                    '<td>'+tasks.data[i].id+'</td>' +
                    '<td>'+tasks.data[i].name_author+'</td>' +
                    '<td>'+tasks.data[i].email_author+'</td>' +
                    '<td>'+tasks.data[i].title+'</td>' +
                    '<td>'+tasks.data[i].description+'</td>' +
                    '<td>'+((tasks.data[i].is_done == 1) ? "Да" : "Нет")+'</td>' +
                    strEdit +
                    '</tr>' + tbody;
            }
            $('<tbody>' + tbody + '</tbody>').insertAfter('thead');
        });
}

function sortByField(logined, event, context)
{
    event.preventDefault();
    $('a').removeClass('active_sort');
    getTaskData(numPage, logined, $(context).attr('data-sort-field'), $(context).attr('data-direction'));
    if ($(context).attr('data-direction') == 'desc') {
        $(context).attr('data-direction', 'asc');
    } else {
        $(context).attr('data-direction', 'desc');
    }
    $(context).addClass('active_sort');
}