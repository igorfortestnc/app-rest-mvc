<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">
    <title>Signin Template for Bootstrap</title>
    <script src="/node_modules/jquery/dist/jquery.js"></script>
    <script src="../../../core/extensions/twbs/bootstrap/dist/js/bootstrap.bundle.js"></script>
    <script src="/node_modules/bootpag/lib/jquery.bootpag.js"></script>
    <script src="/application/views/js/main.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="../../../core/extensions/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="container">

    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <? if (isset($_SESSION['udata'])): ?>
                    <a class="nav-link" href="/main/logout">Выход</a>
                    <? else: ?>
                        <a class="nav-link" href="/main/login">Войти</a>
                    <? endif; ?>
                </li>

            </ul>
            <span style="color: bisque"> BeeJee ( <?= $_SESSION['udata']['username'] ? $_SESSION['udata']['username'] : 'Guest' ?> ) </span>
        </div>
    </nav>

    <?php include $view; ?>
</div> <!-- /container -->

</body>

</html>
